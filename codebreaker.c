#define _POSIX_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>

/* 1st argument: filepath of text file
 * 2nd argument: number of processes
 * 3rd argument: limit on number of tries 
 *
 * EVALUATION REQUIREMENTS
 * This program executes the requirements up to the Grande level.
 * Thus, it implements all the base requirements. These are:
 * A nominated number of processes to use (done via command line)
 * Status updates for the start and finish of each process. 
 * plus:
 * A nominated stopping point (done via command line), and;
 * A signal broadcast to all children if the correct code is found.
 * The requirements not implemented by this program are:
 * A heuristic approach to searching for the password.
 *
 * As for the implementation used, there are of course drawbacks.
 * This program only attempts numeric codes, and as such does not work
 * for the entire alpha-numeric range of passwords.
 *
 * STEP 6 IMPLEMENTATION: 
 * Since the program is designed to search for a numeric password,
 * the program starts attempting codes from 0, and counts up for every attempt.
 * As such, the current code to attempt also doubles as the attempt counter.
 * Therefore, once the current code reaches one below the nominated limit
 * (as the code attempts start from 0), then the program stops. 
 * 
 * STEP 8 IMPLEMENTATION:
 * The program keeps track of all child process id's in an array. Once the
 * code is found, the program is sent back to the parent behaviour. The program
 * then goes through the array and sends a kill signal too all the child processes. 
 * The program then breaks out of the process creation loop and ends operation. 
 * A drawback of this implementation is evident if the number of processes to be used
 * is large. This increases the size of the child id array, which can make it time
 * consuming to loop through to send all seperate kill singals. Some method of being able
 * to send a single signal to all child processes at once (if possible) would be preferred.
 * */

int main(int argc, char * argv[])
{
	int statval;
	int tot_procs;
	int i;
	int j;
	int pass = 0;
	char pass_str[15];
	char * end_ptr;
	int success = 0;
	int c_pid;
	int output;
	int tot_tries;
	int found = 0;
	
	int * child_ids;
	int * pass_vals;

	/* get number of processes needed from args */
	tot_procs = (int) strtol(argv[2], &end_ptr, 10);
	/*get number of tries from args */
	tot_tries = (int) strtol(argv[3], &end_ptr, 10);

	child_ids = calloc(tot_procs, sizeof(int));
	pass_vals = calloc(tot_procs, sizeof(int));

	for(j = 0; j < tot_procs; j++)
	{
		child_ids[j] = -1;	
	}
	/* forking and execution of processes */
	while(!success)
	{
		for(i = 0; i < tot_procs; i++)
		{
			c_pid = fork();
	
			child_ids[i] = c_pid;		
			pass_vals[i] = pass;
			
			if(c_pid == 0)
			{
			 /* child made */
				printf("Process %d beginning execution!\n", getpid());
				sprintf(pass_str, "%d", pass);
				/*printf("%d\n", (int)strlen(pass_str));*/
				/*printf("PASSVALUE: %s\n", pass_str);*/
				execl("fdecrypt", "fdecrypt", argv[1], pass_str, NULL);
			}
			else if(c_pid == -1)
			{
				perror("fork");
				exit(1);
			}
	
			pass++;
			if(pass == tot_tries)
			{
				success = 1;	
				break;
			}
		}
		
		/* parent */
		/*else THIS WAS FOR PUTTING ABOVE IF/ELSEIF STATMENT WITH IT. DID NOT END WELL. INFINITE LOOP.
		{*/
		for(i = (tot_procs - 1); i >= 0; i--)
		{
			wait(&statval);
			if(WIFEXITED(statval))
			{
				printf("Process %d ending execution!\n", child_ids[i]);
				output = WEXITSTATUS(statval);
				/*printf("OUTPUT: %d\n", output);*/
				if(output == 0)	
				{
					printf("** CODE FOUND **\n");
					/*printf("PASS: %d, i: %d\n", pass, i);*/
					printf("The code is: %d\n", pass_vals[(tot_procs - 1) - i]);
					success = 1;
					found = 1;
					/* Kill all other child processes */
					for(j = 0; j < tot_procs; j++)
					{
						if(child_ids[j] == -1)
							continue;
						kill(child_ids[j], SIGTERM);
					}
					break;
				}	
			}
			else
				printf("Child did not terminate with exit!\n");
		}
		/*}*/

	}
	if(found != 1)
		printf("%d attempts have been made, and no code was found!\n", tot_tries);
	free(child_ids);
	free(pass_vals);
	exit(1);	
}
